# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kscreen
pkgver=5.15.5
pkgrel=2
pkgdesc="KDE's screen management software"
arch="all !armhf"
url="https://www.kde.org/workspaces/plasmadesktop/"
license="GPL-2.0"
depends="hicolor-icon-theme"
depends_dev="qt5-qtbase-dev qt5-qtdeclarative-dev libkscreen-dev kdbusaddons-dev kconfig-dev kconfigwidgets-dev ki18n-dev kxmlgui-dev kauth-dev kcoreaddons-dev kcodecs-dev kwidgetsaddons-dev kglobalaccel-dev kdeclarative-dev plasma-framework-dev"
makedepends="$depends_dev extra-cmake-modules"
source="https://download.kde.org/stable/plasma/$pkgver/$pkgname-$pkgver.tar.xz"
subpackages="$pkgname-lang"
options="!check" # Fails due to requiring running X11

build() {
	cmake \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="29776af422e5948f9563b94609472155e2c5ea762ba40d362cf76469f5f9347f56103b819ccbd2a4b9e8b88080c1d573adae6dd6648a36d817ba71eb93ade809  kscreen-5.15.5.tar.xz"
